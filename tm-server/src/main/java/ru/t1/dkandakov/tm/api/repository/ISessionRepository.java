package ru.t1.dkandakov.tm.api.repository;

import ru.t1.dkandakov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
