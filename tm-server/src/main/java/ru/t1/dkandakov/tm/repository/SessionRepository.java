package ru.t1.dkandakov.tm.repository;

import ru.t1.dkandakov.tm.api.repository.ISessionRepository;
import ru.t1.dkandakov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}