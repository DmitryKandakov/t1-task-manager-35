package ru.t1.dkandakov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.dto.request.domain.*;
import ru.t1.dkandakov.tm.dto.response.domain.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(IEndpoint.HOST, IEndpoint.PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, IEndpoint.SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, IEndpoint.SPACE, PART, IDomainEndpoint.class);
    }

    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull final DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataBinarySaveRequest request
    );


    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadJaxBResponse loadDataJsonJaxb(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataJsonLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveJaxBResponse saveDataJsonJaxb(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataJsonSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadJaxBResponse loadDataXmlJaxb(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataXmlLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveJaxBResponse saveDataXmlJaxb(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataXmlSaveJaxBRequest request
    );

    @WebMethod
    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    );

    @WebMethod
    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    );

    @WebMethod
    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = IEndpoint.REQUEST, partName = IEndpoint.REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    );

}