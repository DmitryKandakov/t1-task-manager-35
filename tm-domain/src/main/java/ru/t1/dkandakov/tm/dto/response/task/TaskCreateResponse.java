package ru.t1.dkandakov.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.Task;

public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final Task task) {
        super(task);
    }

}
