package ru.t1.dkandakov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
@Getter
@Setter
public final class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
